<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="css/Iframe.css" />
<link rel="stylesheet" href="utilLib/bootstrap.min.css" type="text/css" media="screen" />
</head>
<body>
<span class="cp_title">新闻管理</span>
<div class="add_cp">
	<a href="AddNews.jsp">+添加新闻</a>
</div>
<div class="table_con">
	<table>
	<tr class="tb_title">
        	<td width="10%">ID</td>
            <td width="30%">标题</td>
            <td width="12%">内容</td>
            <td width="12%">作者</td>
            <td width="10%">时间</td>
            <td width="26%">操作</td>
        </tr>
	<c:forEach var="news" items="${lstNews}" >
    	<tr>
    	   	<td width="10%">${news.newid}</td>
            <td width="30%">${news.title} </td>
            <td width="12%">${news.newcontent}</td>
            <td width="12%">${news.author}</td>
            <td width="10%">${news.writedate}</td>
            <td width="26%">
                <a href="editServlet?newid=${news.newid}" class="del_btn">编辑</a> 
                <a href="viewServlet?newid=${news.newid}" class="del_btn">查看</a> 
                <a href="deleteServlet?newid=${news.newid}" class="del_btn">删除</a> 
            </td>
        </tr>
      </c:forEach>
    </table>
</div>
</body>
</html>