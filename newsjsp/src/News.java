import java.util.Date;


public class News {
 private int newid;
 private String title;
 private String newcontent;
 private String author;
 private Date writedate;
public int getNewid() {
	return newid;
}
public void setNewid(int newid) {
	this.newid = newid;
}
public String getTitle() {
	return title;
}
public void setTitle(String title) {
	this.title = title;
}
public String getNewcontent() {
	return newcontent;
}
public void setNewcontent(String newcontent) {
	this.newcontent = newcontent;
}
public String getAuthor() {
	return author;
}
public void setAuthor(String author) {
	this.author = author;
}
public Date getWritedate() {
	return writedate;
}
public void setWritedate(Date writedate) {
	this.writedate = writedate;
}

}
