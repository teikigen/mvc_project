

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javafx.scene.chart.PieChart.Data;
import jdk.nashorn.api.scripting.ScriptUtils;

@WebServlet("/SaveNewServlet")
public class SaveNewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public SaveNewServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//request.setCharacterEncoding("utf-8");
		//response.setContentType("text/html;charset=utf-8");
		News news=new News();
		news.setNewid(Integer.valueOf(request.getParameter("newid")));
		news.setAuthor(request.getParameter("author"));
		news.setTitle(request.getParameter("title"));
		news.setNewcontent(request.getParameter("newcontent"));
		String strdate=request.getParameter("writedate");
		SimpleDateFormat sDateFormat=new SimpleDateFormat();
		Date date = null;
		try {
			date = sDateFormat.parse(strdate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		news.setWritedate(date);
		NewService service=new NewService();
		service.UpdateNews(news);
		request.getRequestDispatcher("ShowListServlet").forward(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
