import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;

public class NewService {
	public void AddNews(News news) {
		String sql="insert into News (Title,NewsContent,Author)"
		+ "values('"+news.getTitle()+"','"+news.getNewcontent()+"','"+news.getAuthor()+"');";
		SqlHelper.executeUpdate(sql);
	};
	public void DeleteNews(int newsID) {
		String sql="delete from News where NewsID='"+newsID+"'";
		SqlHelper.executeQuery(sql);
	};
	public void UpdateNews(News news) {
		String sqltitle="update News set Title='"+news.getTitle()+"' where NewsID='"+news.getNewid()+"'";
		String sqlauthor="update News set Author='"+news.getAuthor()+"' where NewsID='"+news.getNewid()+"'";
		String sqlcontent="update News set NewsContent='"+news.getNewcontent()+"' where NewsID='"+news.getNewid()+"'";
		SqlHelper.executeUpdate(sqltitle);
		SqlHelper.executeUpdate(sqlauthor);
		SqlHelper.executeUpdate(sqlcontent);
	};
	public List<News> QueryNews() throws SQLException {
		String sql="Select * from News";
		ResultSet rs;
		rs=SqlHelper.executeQuery(sql);
		List<News> lstNews= new ArrayList<News>();
		while (rs.next()) {
			News news=new News();
			news.setNewid(rs.getInt("NewsID"));
			news.setTitle(rs.getString("Title"));
			news.setNewcontent(rs.getString("NewsContent"));
			news.setAuthor(rs.getString("Author"));
			news.setWritedate(rs.getDate("NewsData"));
			lstNews.add(news);
		}
		return lstNews;
	};
	public News GetNews(int newsID) throws SQLException {
		String sql="Select * from News where NewsID='"+newsID+"'";
		ResultSet rs;
		rs=SqlHelper.executeQuery(sql);
		News news = new News();
		if (rs.next()) {
			news.setNewid(rs.getInt("NewsID"));
			news.setTitle(rs.getString("Title"));
			news.setNewcontent(rs.getString("NewsContent"));
			news.setAuthor(rs.getString("Author"));
			news.setWritedate(rs.getDate("NewsData"));
		}
		return news;
	};
}
